import "./App.css";
import { Form, Container, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "@restart/ui/esm/Button";
import React, { useState } from "react";
import axios from "axios";

function App() {
  const baseUrl = "http://localhost:3001/";
  const [_account, setAccount] = useState("");
  const [_name, setName] = useState("");

  async function onClick(event) {
    await axios.post(baseUrl + "profile/create", {
      name: _name,
      account_id: _account,
      total: "0",
    });
  }

  function onChengeAccount(event) {
    setAccount(event.target.value);
  }

  function onChengeName(event) {
    setName(event.target.value);
  }

  return (
    <div className="App">
      <header className="App-header">
        <Container>
          <Row>
            <Col>
              <Form.Label>เลขที่บัญชี</Form.Label>
              <Form.Control
                type="number"
                placeholder="เลขที่บัญชี"
                onChange={onChengeAccount}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col>
              <Form.Label>ชื่อบัญชี</Form.Label>
              <Form.Control
                type="text"
                placeholder="ชื่อบัญชี"
                onChange={onChengeName}
              />
            </Col>
          </Row>
          <br />

          <Row>
            <Col>
              <Button
                variant="outline-primary"
                onClick={onClick}
                to="./TopupMoney.js"
              >
                ถัดไป
              </Button>{" "}
            </Col>
          </Row>
        </Container>
      </header>
    </div>
  );
}

export default App;
