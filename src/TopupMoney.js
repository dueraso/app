import { Form, Container, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "@restart/ui/esm/Button";
import React, { useState } from "react";
import axios from "axios";
var rand = require("random-key");

export default function TopupMoney() {
  const baseUrl = "http://localhost:3001/";
  const [_payType, setPayType] = useState("");
  const [_profileId, setProfileId] = useState("");
  const [_total, setTotal] = useState("");

  async function onClick(event) {
    await axios.post(baseUrl + "topup/create", {
      code_ref: rand.generate(30),
      pay_type_id: _payType,
      profile_id: _profileId,
      total: _total,
    });
  }

  function onChengePayType(event) {
    setPayType(event.target.value);
  }

  function onChengeProfileId(event) {
    setProfileId(event.target.value);
  }

  function onChengeTotal(event) {
    setTotal(event.target.value);
  }

  return (
    <div className="App">
      <header className="App-header">
        <Container>
          <Row>
            <Col>
              <Form.Label>รหัสอ้างอิง</Form.Label>
              <Form.Control
                type="text"
                placeholder="รหัสอ้างอิง"
                onChange={onChengePayType}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col>
              <Form.Label>ชื่อบัญชี</Form.Label>
              <Form.Control
                type="text"
                placeholder="ช่องทางเติม"
                onChange={onChengeProfileId}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col>
              <Form.Label>ชื่อบัญชี</Form.Label>
              <Form.Control
                type="text"
                placeholder="ชื่อบัญชีผู้รับ"
                onChange={onChengeTotal}
              />
            </Col>
          </Row>
          <br />

          <Row>
            <Col>
              <Button variant="outline-primary" onClick={onClick}>
                ถัดไป
              </Button>{" "}
            </Col>
          </Row>
        </Container>
      </header>
    </div>
  );
}
